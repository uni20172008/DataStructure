# The lab repo for CSE221-2019-spring

This is the repository that you should play with during this semester.

- Please read this and per-lab `README.md`s carefully.

## List of labs

Note that the information below may not be up-to-date. Check the main, public
repository for more up-to-date deadlines.

- [main repository](https://class.unicss.org/cse221-2019-spring/cse221-2019-spring)


## Lab0: gitlab

- Out: Feb 28 (Thu)
- Due: Mar 06 (Wed) 11:59pm
- Instruction: [gitlab/README.md](/gitlab/README.md)

```sh
cd gitlab
```


## Lab1: Linked List

- Lead TA: All
- Instruction: [lab1/README.md](/lab1/README.md)
- Out: Mar 12 (Tue)
- Due: Mar 18 (Mon)

```sh
cd lab1
```

## Lab2: Linked List, Stack and Queue

- Lead TA: Yunha Han (diana438@unist.ac.kr, @yunha, Thu 17:00-18:00 @106-303)
- Instruction: [lab2/README.md](lab2/README.md)
- Out: Mar 21 (Thu)
- Due: Apr 03 (Wed) 11:59 pm

```sh
cd lab2
```

## Lab3: Binary Tree

- Lead TA: Kihwan Kim (kh1875@unist.ac.kr, @kihwan)
- Instruction: [lab3/README.md](lab3/README.md)
- Out: Apr 04 (Thu)
- Due: Apr 22 (Mon) 11:59 pm

```sh
cd lab3
```

## Lab4: Priority Queue

- Lead TA: Hwiyeon Kim (gnldus28@unist.ac.kr, @hwiyeon)
- Instruction: [lab4/README.md](lab4/README.md)
- Out: Apr 23 (Tue)
- Due: May 06 (Mon) 11:59 pm

```sh
cd lab3
```






### Notes

- You are expected to be familiar with the concept of `branch` in `git`.
- Grader will create and push branches named `grader-<lab_name>`. For example, `grader-lab0`.
- Grader will first check `submit-labx` branches (e.g., `submit-lab2`) and if it does not exist,
will check the `master` branch.


